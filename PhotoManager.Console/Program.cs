﻿namespace PhotoManager.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string start = @"C:\Users\v-tideli\Desktop\Camera";
            string end = @"C:\Users\v-tideli\Desktop\Resultado";

            if (args.Length > 1)
            {
                start = args[0];
                end = args[1];
            }

            var mover = new PhotoMover();
            mover.Execute(start, end);
        }
    }
}
