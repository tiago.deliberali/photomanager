﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoManager
{
    //http://nicholasarmstrong.com/2010/02/exif-quick-reference/
    public enum ExifTag
    {
        None = 0,
        ModifiedDateTime = 306,
        DateTaken = 36867,
        DateCreated = 36868
    }
}
