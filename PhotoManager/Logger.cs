﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PhotoManager
{
    public class Logger
    {
        private DateTime startDateTime;

        public Logger()
        {
            startDateTime = DateTime.Now;
        }

        public void SaveLog(string path, List<PhotoData> results)
        {
            var log = Path.Combine(path, "resultado.txt");
            using (var file = File.Open(log, FileMode.OpenOrCreate))
            using (var sw = new StreamWriter(file))
            {
                sw.WriteLine("Início em: " + startDateTime.ToString("dd/MM/yyyy hh:mm:ss"));
                sw.WriteLine();

                sw.WriteLine("   Erros:");
                sw.WriteLine("---------------------------------------------------------\n");

                foreach (var item in results.Where(x => x.Error != null))
                {
                    while (item.Error.InnerException != null)
                        item.Error = item.Error.InnerException;

                    sw.WriteLine("{0}\t{1}", item.NumberOfItems, item.LastPath);
                    sw.WriteLine("{0}\n{1}", item.Error.Message, item.Error.StackTrace);
                }

                sw.WriteLine("   Acertos:");
                sw.WriteLine("---------------------------------------------------------\n");

                foreach (var item in results.Where(x => x.Error == null))
                {
                    sw.WriteLine("{0}\t{1}", item.NumberOfItems, item.Path);
                }

                sw.WriteLine("Fim em: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));

                sw.Flush();
            }
        }
    }
}
