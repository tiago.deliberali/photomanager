﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoManager
{
    public class PhotoData
    {
        private string startPath;

        public PhotoData(string path)
        {
            this.Path = path;
        }
        public int NumberOfItems { get; set; }
        public string Path { get; set; }
        public string LastPath { get; set; }
        public Exception Error { get; set; }
    }
}
