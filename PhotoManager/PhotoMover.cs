﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using BKTree;

namespace PhotoManager
{
    public class PhotoMover
    {
        private Regex r = new Regex(":");
        BKTree<PhotoTreeNode> tree;
        private List<PhotoData> results;
        private string startPath;
        private string endPath;
        private bool firstNode = true;


        [DllImport("pHash", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int ph_dct_imagehash(string file, ref UInt64 hash);


        public void Execute(string startPath, string endPath)
        {
            var logger = new Logger();

            this.tree = new BKTree<PhotoTreeNode>();
            this.results = new List<PhotoData>();
            this.startPath = startPath;
            this.endPath = endPath;

            Move(startPath);

            logger.SaveLog(endPath, results);
        }

        private void Move(string currentPath)
        {
            var files = Directory.GetFiles(currentPath);
            var directories = Directory.GetDirectories(currentPath);
            var data = new PhotoData(currentPath);

            try
            {
                foreach (var filePath in files)
                {
                    MoveFile(data, filePath);
                }
            }
            catch (Exception ex)
            {
                data.Error = ex;
                data.NumberOfItems--;
            }

            results.Add(data);

            foreach (var directoryPath in directories)
            {
                Move(directoryPath);
            }
        }

        private void MoveFile(PhotoData data, string filePath)
        {
            data.LastPath = filePath;
            data.NumberOfItems++;

            var destinyPath = GetPath(filePath);

            File.Copy(filePath, destinyPath);
        }

        private string GetPath(string filePath)
        {
            var date = GetDateFromImage(filePath);
            var basePath = string.Empty;
            var path = string.Empty;

            if (ExistSimilarImage(filePath))
                basePath = Path.Combine(endPath, "similar");
            else
                basePath = endPath;

            if (date.HasValue)
                path = Path.Combine(basePath, date.Value.Year.ToString("0000"), date.Value.Month.ToString("00"), Path.GetFileName(filePath));
            else
                path = Path.Combine(basePath, "sem data", Path.GetFileName(filePath));

            return ResolvePath(path);
        }

        private bool ExistSimilarImage(string filePath)
        {
            UInt64 hash = 0;
            int result = ph_dct_imagehash(filePath, ref hash);

            var node = new PhotoTreeNode(hash, filePath);

            if (firstNode)
            {
                tree.add(node);
                firstNode = false;
                return false;
            }
            else
            {
                Dictionary<PhotoTreeNode, Int32> results = tree.query(node, 30);

                if (result == 0)
                {
                    tree.add(node);
                    return false;
                }

                return true;
            }
        }

        private string ResolvePath(string path)
        {
            if (File.Exists(path))
                path = GetNewPath(path);

            var directory = Path.GetDirectoryName(path);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            return path;
        }

        private string GetNewPath(string path)
        {
            return Path.Combine(
                Path.GetDirectoryName(path),
                Path.GetFileNameWithoutExtension(path) + DateTime.Now.Ticks + Path.GetExtension(path));
        }
        
        public DateTime? GetDateFromImage(string path)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                using (Image image = Image.FromStream(fs, false, false))
                {
                    var exifTagList = image.PropertyIdList;
                    var exifTagId = GetExifTagId(exifTagList);

                    PropertyItem propItem = image.GetPropertyItem((int)exifTagId);
                    string dateTaken = r.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);

                    return DateTime.Parse(dateTaken);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private ExifTag GetExifTagId(int[] exifTagList)
        {
            var tag = ExifTag.None;

            if (exifTagList.Contains((int)ExifTag.DateTaken))
                tag = ExifTag.DateTaken;

            else if (!exifTagList.Contains((int)ExifTag.ModifiedDateTime))
                tag = ExifTag.ModifiedDateTime;

            else if (!exifTagList.Contains((int)ExifTag.DateCreated))
                tag = ExifTag.DateCreated;

            return tag;
        }
    }
}
