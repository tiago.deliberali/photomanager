﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BKTree;

namespace PhotoManager
{
    public class PhotoTreeNode : BKTreeNode
    {
        public string Path { get; set; }
        public byte[] Hash { get; set; }

        public PhotoTreeNode(UInt64 hash, string path)
            : base()
        {
            Hash = BitConverter.GetBytes(hash);
            Path = path;
        }

        override protected int calculateDistance(BKTreeNode node)
        {
            return DistanceMetric.calculateHammingDistance(
                this.Hash,
                ((PhotoTreeNode)node).Hash);
        }
    }
}
